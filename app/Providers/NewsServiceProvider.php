<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types = 1);
namespace App\Providers;

use App\Models\News\News;
use App\Repositories\News\NewsRepository;
use App\Services\News\NewsService;
use Illuminate\Support\ServiceProvider;

/**
 * Description of NewsServiceProvider
 *
 * @author luiz
 */
class NewsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(NewsService::class, function($app){
                return new NewsService(new NewsRepository( new News() ) );
            });
    }
}