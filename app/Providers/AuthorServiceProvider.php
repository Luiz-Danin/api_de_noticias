<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types = 1);
namespace App\Providers;

use App\Models\Author\Author;
use App\Repositories\Author\AuthorRepository;
use App\Services\Author\AuthorService;
use Illuminate\Support\ServiceProvider;
/**
 * Description of AuthorServiceProvider
 *
 * @author luiz
 */
class AuthorServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(AuthorService::class, function($app){
            return new AuthorService(new AuthorRepository( new Author() ) );
        });
            
    }
}
