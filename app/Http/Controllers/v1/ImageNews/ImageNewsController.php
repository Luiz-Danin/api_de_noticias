<?php

declare (strict_types = 1);
namespace App\Http\Controllers\v1\ImageNews;

use App\Http\Controllers\AbstractController;
use App\Services\ImageNews\ImageNewsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ImageNewsController  extends AbstractController
{
    protected $seachFields = [];
    
    public function __construct(ImageNewsService $service) 
    {
        parent::__construct($service);
    }
    
    public function findByNews(Response $response, int $news): JsonResponse
    {
        try {
            
            $result = $this->service->findByNews($news);
            $response = $this->successResponse($result);
        } catch (Exception $ex) {
            $response = $this->erroResponse($e);
        }
        
        return response()->json($response, $response['status_code']);
    }
    
    public function deleteByNews(Response $response, int $news): JsonResponse
    {
        try {
            $result['deletado'] = $this->service->deleteByNews($news);
            $response = $this->successResponse($result);
        } catch (Exception $ex) {
            $response = $this->erroResponse($e);
        }
        
        return response()->json($response, $response['status_code']);
    }
} 