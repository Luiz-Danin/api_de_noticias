<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types=1);
namespace App\Http\Controllers\v1\News;

use App\Http\Controllers\AbstractController;
use App\Services\News\NewsService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Exception;
use Illuminate\Http\Response;
/**
 * Description of NewsController
 *
 * @author luiz
 */
class NewsController extends AbstractController
{
    protected array $searchFields = ['titulo', 'slug','subtitulo'];
    
    public function __construct(NewsService $service) 
    {
        parent::__construct($service);
    }
    
    public function findByAuthor(Request $request, int $author): JsonResponse
    {
        try{
            $limit = (int)$request->get('limit', 10);
            $orderBy = $request->get('order_by',[]);
            
            if( !empty( $orderBy ) )
            {
                $orderBy = OrderByHelper::treatOrderBy($orderBy);
            }
            
            $result = $this->service->findByAuthor($author, $limit, $orderBy);
            
            $response = $this->successResponse($result, Response::HTTP_PARTIAL_CONTENT);
        } catch (Exception $e){
            $response = $this->erroResponse($e);
        }
        
        return response()->json($response, $response['status_code']);
    }
    
    public function findBy(Request $request, string $param):JsonResponse
    {
        try{
            $result = $this->service->findBy($param);
            $response = $this->successResponse($result);
        } catch (Exception $e){
            $response = $this->erroResponse($e);
        }
        
        return response()->json($response, $response['status_code']);
    }
    
    public function deleteBy(Request $request, string $param): JsonResponse
    {
        try {
            $result['deletado'] = $this->service->deleteBy($param);
            $response = $this->successResponse($result);
        } catch (Exception $e) {
            $response = $this->erroResponse($e);
        }
        
        return response()->json($response, $response['status_code']);
    }
    
    public function deleteByAuthor(Request $request, int $author):JsonResponse
    {
        try {
            
            $result['deletado'] = $this->service->deleteByAuthor($author);
            $response = $this->successResponse($result);
        } catch (Exception $ex) {
            $response = $this->erroResponse($e);
        }
         
        return response()->json($response, $response['status_code']);
    }
}
