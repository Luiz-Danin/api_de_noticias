<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AuthorController
 *
 * @author luiz
 */
declare (strict_types = 1);
namespace App\Http\Controllers\v1\Author;

use App\Http\Controllers\AbstractController;
use App\Services\Author\AuthorService;

class AuthorController extends AbstractController
{
    protected array $searchFields = ['nome', 'sobrenome'];
    
    public function __construct(AuthorService $service)
    {
        //$this->service = $service;
        parent::__construct($service);
    }
}
