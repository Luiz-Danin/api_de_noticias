<?php
declare (strict_types=1);
namespace App\Http\Controllers;

use App\Services\ServiceInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Helpers\OrderByHelper;

abstract class AbstractController extends BaseController implements ControllerInterface
{
    protected ServiceInterface $service;
    protected array $searchFields = [];

    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }
    
    protected function successResponse(array $data, int $statusCode = Response::HTTP_OK): array
    {   
        return [
                    'status_code' => $statusCode,
                    'data' => $data
               ];
    }
    
    protected function erroResponse(Exception $e, int $status_code = Response::HTTP_BAD_REQUEST): array
    {
        return [
                    'status_code' => $status_code,
                    'error' => true,
                    'erro_description' => $e->getMessage()
               ];
    }

    public function create(Request $request): JsonResponse
    {
        try{
            //$req = $this->validate($request, $this->service);
            $result = $this->service->create( $request->all() );
            $response = $this->successResponse($result, Response::HTTP_CREATED);
        } catch (Exception $e){
            
            $response = $this->erroResponse($e);
        }
        
        return response()->json($response, $response['status_code']);
    }
    
    public function findAll(Request $request): JsonResponse 
    {
         try{
             $limit = (int)$request->get('limit', 10);
             $orderBy = $request->get('order_by', []);
             
             if( !empty( $orderBy ) )
             {
                 $orderBy = OrderByHelper::treatOrderBy($orderBy);
             }
             
             $searchString = $request->get('q', '');
             
             if( !empty( trim($searchString) ) )
             {
                 $result = $this->service->searchBy(
                         $searchString, 
                         $this->searchFields, 
                         $limit, 
                         $orderBy
                         );
             }else{
                 $result = $this->service->findAll($limit, $orderBy);
             }
             
             $response = $this->successResponse($result, Response::HTTP_PARTIAL_CONTENT);
         } catch ( Exception $e)
         {
             $response = $this->erroResponse($e);
         }
         
         return response()->json($response, $response['status_code']); 
    }
    
    public function findOneBy(Request $request, int $id): JsonResponse 
    {
        try{
            $result = $this->service->findOneBy($id);
            $response = $this->successResponse($result);
        } catch (Exception $e){
            
            $response = $this->erroResponse($e);
        }
        
        return response()->json($response, $response['status_code']);
    }
    
    public function editBy(Request $request, string $param): JsonResponse 
    {
        try{
            $result['registro_alterado'] = $this->service->editBy($param, $request->all() );
            $response = $this->successResponse($result);
        }
        catch (Excption $e){
            $result = $this->erroResponse($e);
        }
        
        return response()->json($response, $response['status_code']);
    }
    
    public function delete(Request $request, int $id): JsonResponse 
    {
        try {
            $result['registro_deletado'] = $this->service->delete($id);
            $response = $this->successResponse($result);
        } catch (Exception $e) {
            $response = $this->erroResponse($e);
        }
        
        return response()->json($response, $response['status_code']);
    }
}