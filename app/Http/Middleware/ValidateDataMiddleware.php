<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare(strict_types=1);
namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
/**
 * Description of ValidateDataMiddleware
 *
 * @author luiz
 */
class ValidateDataMiddleware 
{
    public function handle(Request $request, \Closure $next)
    {
        $validate = $this->validate($request);
        
        $response = [
                    'status_code' => 400,
                    'error' => true,
                    'error_message' => 'Dados Inválidos',
                    'error_description' => $validate->messages()
                   ];
        
        if($validate->passes()) {
            
            $response = $next($request);
        }
        
        return $response;
    }
    
    private function defineModel(string $namespace): ?Model
    {
        $model = null;
        
        if( class_exists($namespace) ) {
            $model = new $namespace;
        }
        
        return $model;
    }
    
    private function validate(Request $request) 
    {
        $alias = $request->route()[1]['as'];
        $model = $this->defineModel($alias);
        
        if( empty($model) ) {
            throw new \InvalidArgumentException('A model '.$model.' não existe');
        }
        //dd($model->rules);
        return Validator::make(
                $request->toArray(), 
                $model->rules
        );
    }
    
}
