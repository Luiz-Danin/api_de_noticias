<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types=1);
namespace App\Models\Author;

use App\Models\News\News;
use Illuminate\Database\Eloquent\Model;
/**
 * Description of Author
 *
 * @author luiz
 */
class Author extends Model
{
    protected $table = 'autores';
    
    protected $fillable = 
            [
                'nome',
                'sobrenome',
                'email',
                'senha',
                'sexo',
                'ativo',
                'criado_em'
            ];
    
    protected $hidden = ['senha'];
    
    public $timestamps = false;
    
    public array $rules = 
            [
                'nome' => 'required|min:2|max:45|alpha',
                'sobrenome' => 'required|min:2|max:60|alpha',
                'email' => 'required|email|unique:Author,email|max:100|email:rfc,dns',
                'senha' => 'required|between:6,12',
                'sexo' => 'required|alpha|max:1',
            ];
    
    public function News()
    {
        return $this->hasMany(News::class);
    }
}
