<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types=1);

namespace App\Models\News;


use App\Models\Author\Author;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\ImageNews\ImageNews;
use Illuminate\Database\Eloquent\Model;
/**
 * Description of News
 *
 * @author luiz
 */
class News extends Model
{
    use Sluggable;
    protected $table = 'noticias';
    
    
    protected $fillable = 
            [
                'autor_id',
                'titulo',
                'subtitulo',
                'descricao',
                'publicada_em',
                'slug',
                'ativo',
                'criado_em'
            ];
    
    const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'alterado_em';
    const DELETED_AT = 'deletado_em';
    
    public $timestamps = false;
    
    public array $rules = 
            [
                'autor_id'  => 'required|numeric|',
                'titulo' => 'required|min:20|max:100|alpha_num',
                'subtitulo' => 'required|min:20|max:155|alpha_num',
                'descricao' => 'required|min:100',
                'slug' => 'required',
            ];
    
    public static function boot() 
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{self::CREATED_AT} = $model->freshTimestamp();
        });
        static::updating(function ($model) {
            $model->{self::UPDATED_AT} = $model->freshTimestamp();
        });
        static::deleted(function ($model) {
            $model->{self::DELETED_AT} = $model->freshTimestamp();
        });
    }

    public function images()
    {
        return $this->hasMany(ImageNews::class);
    }
    
    public function author()
    {
        return $this->belongsTo(Author::class);
    }
    
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['titulo', 'subtitulo'],
                'onUpdate' => true
            ]
        ];
    }
}
