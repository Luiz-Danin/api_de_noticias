<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types=1);

namespace App\Models\ImageNews;

use Illuminate\Database\Eloquent\Model;
use App\Models\News\News;

/**
 * Description of ImageNews
 *
 * @author luiz
 */
class ImageNews extends Model
{
    protected $table = 'imagens_noticias';
    
    protected $fillable = 
            [
                'noticia_id',
                'imagem',
                'descricao',
                'ativo',
                'criado_em',
                'alterado_em'
            ];
    
    public $timestamps = false;
    
    public $rules = 
            [
                'noticia_id' => 'required|numeric',
                'imagem' => 'required',
                'descricao' => 'required|min:10|max:255',
            ];
    
    public function news()
    {
        return $this->belongsTo(News::class);
    }
}
