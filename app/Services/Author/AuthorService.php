<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types=1);
namespace App\Services\Author;

use App\Services\AbstractServices;

/**
 * Description of AuthorService
 *
 * @author luiz
 */
class AuthorService extends AbstractServices
{
    
    public function create(array $data): array 
    {
       $data['senha'] = encrypt($data['senha']);
       return $this->repository->create($data);
    }
    
    public function editBy(string $param, array $data): bool 
    {
        $data['senha'] = encrypt($data['senha']);
        return $this->repository->editBy($param, $data);
    }
}
