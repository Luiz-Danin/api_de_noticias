<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types=1);
namespace App\Services\News;

use App\Services\AbstractServices;
//use Illuminate\Support\Str;

/**
 * Description of NewsService
 *
 * @author luiz
 */
class NewsService extends AbstractServices
{
    public function findByAuthor(int $author_id, int $limit=10, $orderBy = []): array
    {
        return $this->repository->findByAuthor($author_id, $limit, $orderBy);
    }
    
    public function findBy(string $param):array
    {
        return $this->repository->findBy($param);
    }
    
    public function deleteBy(string $param)
    {
        return $this->repository->deleteBy($param);
    }
    
    public function deleteByAuthor(int $author_id):bool
    {
        return $this->repository->deleteByAuthor($author_id);
    }
    
    /*
     * 
    public function create(array $data): array 
    {
       //$data['slug'] = Str::slug($data['titulo'].''.$data['subtitulo']);
       $data['slug'] = Str::of($data['titulo'].' '.$data['subtitulo'])->slug('-');
       return $this->repository->create($data);
    }
    */
    
    /*
     * 
    public function editBy(string $param, array $data): bool 
    {
        //$data['slug'] = Str::slug($data['titulo'].''.$data['subtitulo']);
        $data['slug'] = Str::of($data['titulo'].' '.$data['subtitulo'])->slug('-');
        return $this->repository->editBy($param, $data);
    }
     */
    
}