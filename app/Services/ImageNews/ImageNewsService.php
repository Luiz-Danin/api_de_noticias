<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types=1);
namespace App\Services\ImageNews;

use App\Services\AbstractServices;
use InvalidArgumentException;

/**
 * Description of ImageNewsService
 *
 * @author luiz
 */
class ImageNewsService extends AbstractServices
{
    public function findByNews(int $newsId): array
    {
        return $this->repository->findByNews($newsId);
    }
    
    public function deleteByNews(int $newsId): bool
    {
        return $this->repository->deleteByNews($newsId);
    }
    
    public function create(array $data): array 
    {
        $this->isImage($data['imagem']);
        $data['imagem'] = base64_encode( file_get_contents( $data['imagem'] ) );
        
        return $this->repository->create($data);
    }
    
    public function editBy(string $param, array $data): bool 
    {
        $this->isImage($data['imagem']);
        $data['imagem'] = base64_encode( file_get_contents( $data['imagem'] ) );
        
        return $this->repository->editBy($param, $data);
    }
    
    private function isImage(string $string): bool
    {
        $imageArray = getimagesize($string);
        $in_array = in_array($imageArray[2], [IMAGETYPE_JPEG,IMAGETYPE_PNG]);
        
        if(!$in_array){
            throw new InvalidArgumentException('Arquivo de Imagem Inválida');
        }
        
        return $in_array;
    }
}
