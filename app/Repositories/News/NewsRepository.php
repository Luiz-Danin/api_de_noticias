<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types=1);

namespace App\Repositories\News;

use App\Models\ImageNews\ImageNews;
use App\Repositories\AbstractRepository;
/**
 * Description of NewsRepository
 *
 * @author luiz
 */
class NewsRepository extends AbstractRepository
{
    public function findByAuthor(int $authorId, int $limit, array $orderBy = []): array
    {
        $results = $this->model::where('autor_id', $authorId);
        
        foreach ($orderBy as $key => $value)
        {
            if( strstr($key, '-') )
            {
                $key = substr($key, 1);
            }
            
            $results->orderBy($key, $value);
        }
        
        return $results->paginate($limit)
                ->appends([
                    'order_by'=> implode(',', array_keys($orderBy))
                ])->toArray();
    }
    
    public function findBy(string $param): array
    {
        $query = $this->model::query();
        
        if (is_numeric($param)) {
            $news = $query->findOrFail($param);
        } else {
            $news = $query->where('slug', $param)
                ->get();
        }

        return $news->toArray();
    }
    
    public function editBy(string $param, array $data): bool
    {
        if( is_numeric($param) )
        {
            $news = $this->model::find($param);
        } else {
            $news = $this->model::where('slug', $param);
        }
        
        return $news->update($data) ? true : false;   
    }
    
    public function deleteBy( string $param):bool
    {
        if( is_numeric($param) )
        {
            $news = $this->model::destroy($param);
        } else {
            $news = $this->model::where('slug', $param)->delete();
        }
        
        return $news ? true : false;
    }
    
    public function deleteByAuthor(int $authorId): bool
    {
        $news = $this->model::where('author', $authorId)->delete();
        
        return $news ? true : false;
    }
    
    public function findNewsAndImageById()
    {
        $news = $this->model()->image()->findOrFail($id);
        
        return $news->toArray();
    }
}
