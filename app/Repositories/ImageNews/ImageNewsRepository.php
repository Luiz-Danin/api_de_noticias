<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types=1);
namespace App\Repositories\ImageNews;

use App\Repositories\AbstractRepository;
/**
 * Description of ImageNewsRepository
 *
 * @author luiz
 */
class ImageNewsRepository extends AbstractRepository
{
    
    public function findByNews(int $newsId): array 
    {
        return $this->model::where('noticia_id', $newsId)->get()->toArray();
    }
    
    public function deleteByNews(int $newsId): bool
    {
        $result = $this->model::where('noticia_id', $newsId)->delete();
        
        return $result ? true : false;
    }
}
