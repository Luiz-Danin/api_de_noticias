<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare(strtict_types=1);
namespace App\Repositories;

/**
 *
 * @author luiz
 */
interface RepositoryInterface
{
    public function create(array $data):array;
    
    public function findAll(int $limit = 10, array $orderBy = []): array;
    
    public function findOneBy(int $id): array;
    
    public function editBy(string $param, array $data): bool;
    
    public function delete(int $id): bool;
    
    public function searchBy(string $string, array $searchFields, int $limit, array $orderBy): array;
}
